const config = {
	SERVER: {
		HOST: process.env.IP || 'localhost',
		PORT: process.env.PORT || 3000
	},
	DB: {
		HOST: process.env.IP || 'localhost',
		NAME: 'newsdb' 
	},
	API: {
		KEY: '8bad7979-67a1-46cb-9819-a08a3801976f',
		SECTIONS: ['world', 'sport', 'football', 'commentisfree', 'culture', 'business', 'lifeandstyle', 'fashion', 'environment', 'technology', 'travel'],
		FIELDS: ['headline', 'trailText', 'byline', 'main', 'body', 'shortUrl', 'thumbnail']
	}
}

module.exports = config;