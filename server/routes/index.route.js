"use strict";

var router     = require('express').Router();
var controller = require('../controllers/index.controller');

router.get('/', (req, res) => 
	controller.reserve());

module.exports = router;